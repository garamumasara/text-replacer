use std::io;

const EOF: &str = ":q";

fn main() {
    let mut lines = Vec::new();
    eprintln!("if entered \"{}\" then end", EOF);

    loop {
        let mut buf = String::new();
        io::stdin().read_line(&mut buf).unwrap();
        if buf.trim_end() == EOF {
            break;
        }
        lines.push(buf.trim_end().to_owned());
    }

    output(&lines);
}

#[derive(Debug, Clone, PartialEq, Hash, Eq)]

enum Kind {
    Verb,
    VerbPhrase,
    Noun,
    NounPhrase,
    NounClause,
    Adjective,
    AdjectivePhrase,
    AdjectiveClause,
    Adverb,
    AdverbPhrase,
    AdverbClause,
    Preposition,
    AuxiliaryVerb,
    Pronoun,
    Article,
    Conjunction,
    Interjection,
    RelativePronoun,
    RelativeAdverb,
    SetPhrase,
    SetSentence,
    Prefix,
    Suffix,
    Conversation,
}

impl Kind {
    fn get_str(&self) -> &str {
        match self {
            Self::Verb => "動詞 ",
            Self::VerbPhrase => "動詞句 ",
            Self::Noun => "名詞 ",
            Self::NounPhrase => "名詞句 ",
            Self::NounClause => "名詞節 ",
            Self::Adjective => "形容詞 ",
            Self::AdjectivePhrase => "形容詞句 ",
            Self::AdjectiveClause => "形容詞節 ",
            Self::Adverb => "副詞 ",
            Self::AdverbPhrase => "副詞句 ",
            Self::AdverbClause => "副詞節 ",
            Self::Preposition => "前置詞 ",
            Self::AuxiliaryVerb => "助動詞 ",
            Self::Pronoun => "代名詞 ",
            Self::Article => "冠詞 ",
            Self::Conjunction => "接続詞 ",
            Self::Interjection => "間投詞 ",
            Self::RelativePronoun => "関係代名詞 ",
            Self::RelativeAdverb => "関係副詞 ",
            Self::SetPhrase => "定型句 ",
            Self::SetSentence => "定型文 ",
            Self::Prefix => "接頭辞 ",
            Self::Suffix => "接尾辞 ",
            Self::Conversation => "会話 ",
        }
    }
}

#[derive(Debug, Clone, PartialEq, Hash, Eq)]
struct Rp(&'static str, Kind);

impl Rp {
    fn len(&self) -> usize {
        self.0.len()
    }
    fn str(&self) -> &str {
        self.0
    }
    fn kind(&self) -> &str {
        self.1.get_str()
    }
}

#[derive(Debug, Clone, PartialEq, Hash, Eq)]
enum Parsed<'a> {
    Str(&'a str),
    Kind(Rp),
}

impl<'a> Parsed<'a> {
    fn get_str(&self) -> &str {
        match self {
            Self::Str(s) => s,
            Self::Kind(ident) => ident.kind(),
        }
    }
}

fn output(lines: &Vec<String>) {
    use Kind::*;
    let v = vec![
        Rp("１２１", SetSentence),
        Rp("９１", RelativeAdverb),
        Rp("４１", AdverbPhrase),
        Rp("３１", AdjectivePhrase),
        Rp("２１", NounPhrase),
        Rp("１６", Article),
        Rp("１５", Prefix),
        Rp("１４", Suffix),
        Rp("１３", Conversation),
        Rp("１２", SetPhrase),
        Rp("１１", VerbPhrase),
        Rp("１０", Interjection),
        Rp("９", RelativePronoun),
        Rp("８", Conjunction),
        Rp("７", AuxiliaryVerb),
        Rp("６", Preposition),
        Rp("５", Pronoun),
        Rp("４", Adverb),
        Rp("３", Adjective),
        Rp("２", Noun),
        Rp("１", Verb),
    ];

    let mut result = Vec::new();

    lines.iter().enumerate().for_each(|(num, line)| {
        result.push(Vec::new());
        result[num].push(Parsed::Str(&line));

        v.iter().for_each(|kind| {
            let mut temp = Vec::new();
            result[num].iter().for_each(|line| match line {
                Parsed::Str(s) => {
                    let s_u8 = s.as_bytes();
                    let mut start = 0;
                    let mut index = kind.len();
                    while index <= s_u8.len() {
                        let slice = &s_u8[index - kind.len()..index];
                        if slice == kind.str().as_bytes() {
                            let front = index - kind.len();
                            if start < front {
                                temp.push(Parsed::Str(&s[start..front]));
                            }
                            temp.push(Parsed::Kind(kind.clone()));
                            start = index;
                            index += kind.len();
                        } else {
                            index += 1;
                        }
                    }
                    if start < s.len() {
                        temp.push(Parsed::Str(&s[start..]));
                    }
                }
                Parsed::Kind(_) => {
                    temp.push(line.clone());
                }
            });
            result[num].clear();
            result[num] = temp;
        });
    });

    result.iter().for_each(|line| {
        line.iter().for_each(|phrase| {
            print!("{}", phrase.get_str());
        });
        println!();
    });
}
